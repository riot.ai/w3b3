# 	Webby - Decentralized Internet Dashboard

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Webby allows users to easily onboard to the new Decentralized Internet.  It includes a few basic tools that 
allow users to get started quickly and securely.  

Our aim is to displace centralized systems of control by building bridges between familiar systems and new ones. Webby is only one piece of that puzzle, and we are proud to add one more way that users can slowly, but surely, take back control of their digital lives.


### Webby Dapps

Webby Dapps allows users to explore new applications, and save their favorties for quick and easy access.

### Webby Contacts

A contact manager should be about more than who's information you can save.  Its about what you *share* with others.  Webby Contacts
provides an easy to use Profile manager so that users can customize the information they share.  At work?  Share only your 
work-related contact details.  At a conference?  Select as many details as you wish to share with other conference attendees.
Profiles are customizable, and you'll always see what you're sharing before you do it.

### Webby Files

A simple file manager that allows you to encrypt and retrieve documents whereever you are.  Photos, documents, music and videos up to 25 MB
are saved with your unique private key.  No cloud provider snooping.

## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev -m pwa
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
yarn run build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).


## Installing your own instance

1. git clone
2. 