export default class Email {
  constructor(options = {}) {
    this.type = options.type || "";
    this.address = options.address || "";
  }
}
