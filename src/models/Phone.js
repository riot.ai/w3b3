export default class Phone {
  constructor(options = {}) {
    this.type = options.type || "";
    this.number = options.number | "";
  }
}
