// import something here
import * as blockstack from "blockstack";
import { UserSession, AppConfig } from "blockstack";

const userSession = new UserSession({
  appConfig: new AppConfig(["store_write", "publish_data", "email"])
});

// "async" is optional
export default ({ router }) => {
  // something to do
  // Vue.prototype.$blockstack = blockstack;
  // Vue.prototype.$userSession = userSession;

  // trying to use router per example at
  // https://quasar.dev/quasar-cli/cli-documentation/boot-files#Router-authentication
  //
  router.beforeEach((to, from, next) => {
    if (to.matched.some(page => page.meta.requiresAuth)) {
      try {
        // if blockstack.userSession.sessionStore is deleted, it fails with only a console error
        // trap that and force login if its not found
        let loggedIn = userSession.isUserSignedIn();
        if (loggedIn) {
          next();
        } else {
          next("/");
        }
      } catch {
        next("/");
      }
    } else {
      next();
    }
  });
};

export { userSession, blockstack };
